package com.example.anmol.touchhandling;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public class MainActivity extends AppCompatActivity {
    HashMap <Integer, View> hm;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        hm = new HashMap<>();
        final RelativeLayout rl = (RelativeLayout) findViewById(R.id.rel);
        rl.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int id = event.getActionMasked();
                if(id == MotionEvent.ACTION_DOWN){
                    View newView = new View(MainActivity.this);
                    float x = event.getX();
                    float y = event.getY();
                    int newPointerId = event.getPointerId(event.getActionIndex());
                    newView.setLayoutParams(new LinearLayout.LayoutParams(200,200));
                    newView.setBackgroundColor(Color.BLACK);
                    newView.setX(x - 100);
                    newView.setY(y - 100);
                    rl.addView(newView);
                    hm.put(newPointerId, newView);
                }else if(id == MotionEvent.ACTION_POINTER_DOWN){
                    View newView = new View(MainActivity.this);
                    int newPointerIndex = event.getActionIndex();
                    int newPointerId = event.getPointerId(newPointerIndex);
                    float x = event.getX(newPointerId);
                    float y = event.getY(newPointerId);
                    newView.setLayoutParams(new LinearLayout.LayoutParams(200,200));
                    newView.setBackgroundColor(Color.BLACK);
                    newView.setX(x - 100);
                    newView.setY(y - 100);
                    rl.addView(newView);
                    hm.put(newPointerId, newView);
                }else if(id == MotionEvent.ACTION_POINTER_UP || id == MotionEvent.ACTION_UP){
                    int pointerIndex = event.getActionIndex();
                    int pointerId = event.getPointerId(pointerIndex);
                    if(hm.get(pointerId) != null) {
                        View newView = hm.get(pointerId);
                        ((ViewGroup) newView.getParent()).removeView(newView);
                    }
                    hm.remove(pointerId);
                }else if(id == MotionEvent.ACTION_MOVE){

                    for(int i = 0;i < event.getPointerCount(); i++){
                        int pid = event.getPointerId(i);
                        View v1 = hm.get(pid);
                        v1.setX(event.getX(i) - 100);
                        v1.setY(event.getY(i) - 100);
                    }
                }
                return true;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
